import psycopg2
import time
from typing import Optional,List
from fastapi import FastAPI, Response,status,HTTPException,Depends
from fastapi.param_functions import Body
from pydantic import BaseModel
from . import models
from random import randrange
from psycopg2.extras import RealDictCursor
from sqlalchemy.orm import Session
from . import utils
from . import schemas


from .database import engine, get_db



models.Base.metadata.create_all(bind=engine)

app = FastAPI()

while True:

    try:
        conn = psycopg2.connect(host='localhost'
                                ,database='fastapi'
                                ,user='postgres'
                                ,password='password123'
                                ,cursor_factory=RealDictCursor)
        cursor = conn.cursor()
        print("Database connectino was succssfull")
        break
    except Exception as error:
        print("Connection Failed ")
        print("Error: ",error)
        time.sleep(2)



@app.get("/")
def root():
    return {"message": "Welcome to my API new"}


@app.get("/posts",response_model=List[schemas.Post])
def get_posts(db: Session = Depends(get_db)):
    #cursor.execute(""" SELECT * FROM posts """)
    #posts = cursor.fetchall()
    posts = posts = db.query(models.Post).all()
    
    return posts

@app.get("/posts/{id}",response_model=schemas.Post)
def get_posts(id: int,db: Session = Depends(get_db)): #saying i want this to be integer ( validation)
    #cursor.execute(""" SELECT * FROM posts WHERE id=%s """,(str(id)))
    #post=cursor.fetchone()
    post = db.query(models.Post).filter(models.Post.id == id).first()
    #print(post)
    
    
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"post with id: {id} was not found")
    return  post

@app.post("/posts",status_code=status.HTTP_201_CREATED,response_model=schemas.Post)
def create_posts(post: schemas.PostCreate,db: Session = Depends(get_db)):
   # cursor.execute(""" INSERT INTO posts (title,content,published) VALUES (%s,%s,%s) RETURNING *""",(post.title,post.content,post.published))
   # new_post = cursor.fetchone()
   # conn.commit()
    new_post = models.Post(**post.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    return new_post

@app.delete("/posts/{id}",status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id: int,db: Session = Depends(get_db)):
    #delete post
    #cursor.execute(""" DELETE FROM posts WHERE id = %s RETURNING *""",(str(id)))
    #delete_post=cursor.fetchone()
    #conn.commit()
    post_query = db.query(models.Post).filter(models.Post.id == id)
    

    if post_query.first() == None:
       raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                        detail=f'psost with id={id} does not exists' ) 

    post_query.delete(synchronize_session=False)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)

@app.put("/posts/{id}",status_code=status.HTTP_202_ACCEPTED,response_model=schemas.Post)
def update_post(id: int,post: schemas.PostCreate,db: Session = Depends(get_db)):
    
   # cursor.execute(""" UPDATE posts SET title=%s,content=%s,published=%s WHERE id=%s RETURNING *""",(post.title,post.content,post.published,str(id)))
    #updated_post=cursor.fetchone()
   # conn.commit()
    post_query = db.query(models.Post).filter(models.Post.id == id)
    post_new = post_query.first()
    if post_new == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                        detail=f'psost with id={id} does not exists' )
    
    post_query.update(post.dict(),synchronize_session=False)
     
    db.commit()

    return post_query.first()

@app.post("/users",status_code=status.HTTP_201_CREATED,response_model= schemas.UserOut)
def create_user(user: schemas.UserCreate,db: Session = Depends(get_db)):
    #hash password
   
    user.password=utils.hash(user.password)
    new_user = models.User(**user.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    
    return new_user

@app.get('/users/{id}')
def get_user(id: int,db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.id == id).first()
    print(id)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"User with id: {id} does not exists")