from psycopg2 import connect
from pydantic import BaseModel,EmailStr
from datetime import datetime


class PostBase(BaseModel):
    title: str
    content: str
    published: bool = True

class PostCreate(PostBase):
    pass

#post output format
class Post(PostBase):

    id:int
    created_at: datetime

    class Config:
        orm_mode=True
#input format
class UserCreate(BaseModel):
    email: EmailStr
    password: str
    
#output format
class UserOut(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime
    class Config:
        orm_mode = True
